<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function register()
	{
    $sql = $this->db->query("INSERT INTO usuarios ('nombre', 'apellido', 'correo', 'contraseña') VALUES (?, ?, ?, ?)");

    $this->db_default->query($sql, array($var1, $var2, $var3, $var4, $var5));

		$this->load->view('register');
  }

	public function login()
	{
    $username = $this->input->get('u');
    $password = $this->input->get('p');
    $data['username'] = $username;
    $data['password'] = $password;
		$this->load->view('login',$data);
  }
}
